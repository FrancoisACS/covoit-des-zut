<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div class="blue lighten-4 row menu" style="padding-top:90px;">

    <div class="col s8 center-align card-panel " style="background-color: #027A70;">
        <h5 class="valign-wrapper left-align">Destination départ / arrivée : </h5>
        <h5 class="valign-wrapper left-align">Heure d'arrivée : </h5>
        <h5 class="valign-wrapper left-align">Proposer par : </h5>
        <h5 class="valign-wrapper left-align">information : </h5>
    </div>
    <div class="col s3 center-align card-panel " style="background-color: #027A70;">

        <form action="{{ url('reserve') }}" method="POST">


            <p>Nombre de places souhaitées</p>
            <input type="number" id="number" name="number" min="1" max="6">
            <button class="btn waves-effect waves-light" type="submit" name="action">Inscription
                <input type="hidden" name="_token" value="iIjW9PMNsV6VKT2sIc16ShoTf6SdYVZolVUGsxDI">
                <i class="material-icons right">send</i>
            </button>
            {{ csrf_field() }}
        </form>


    </div>

</div>

</body>
</html>
