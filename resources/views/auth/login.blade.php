@extends('layouts.footer')

@extends('layouts.app')

@section('content')
    <div class="blue lighten-4 center">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row connect_hub">
                <label for="email" class="col s12 center">{{ __('E-Mail') }}</label>
                <div class="col s12 center">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <label for="password" class="col s12 center">{{ __('Mot de Passe') }}</label>
                <div class="col s12 center">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <button type="submit" class="red darken-3 btn btn-primary">
                        {{ __('Se Connecter') }}
                    </button>
                    @if (Route::has('password.request'))
                        <a class="red darken-3 btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Mot de Passe Oublié ?') }}
                        </a>
                    @endif
                </div>
            </div>
        </form>
    </div>
    <div class="partenaires col s12">
        <h4 class="round_font">Voici nos partenaires :</h4>
        <div class="col s4">
            <div class="card horizontal col s4">
                <div class="card-image">
                    <img src="http://www.montigny-aux-amognes.fr/documents/portal52/.photos-commune-024_m.jpg">
                </div>
                <div class="card-stacked col s4">
                    <div class="card-action">
                        <a href="http://www.montigny-aux-amognes.fr/">Commune de Montigny-aux-Amognes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('footer')
@endsection


