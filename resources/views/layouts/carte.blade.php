<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- leaflet -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />


    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
            integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
            crossorigin=""></script>

    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>

    <div id="map">



    </div>


    <script type="text/javascript">
        var map = L.map('map').setView([47.0167, 3.2833], 12);
        L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png',{minZoom:12},{maxZoom:18}).addTo(map);


        L.Routing.control({
            waypoints: [
                L.latLng(47.02964256843144, 3.30091367277646),
                L.latLng(46.992078, 3.143066),

            ],
            draggable: false,
            language: 'fr',
            routeWhileDragging: true,
        }).addTo(map);



        //            var marker = L.marker([47.0167, 3.2833]).addTo(map);
        //
        //            var marker2 = L.marker([46.990896, 3.162845]).addTo(map);




    </script>

    <title>Carte</title>

    <main class="py-4">
        @yield('mapContent')
    </main>

</head>
</html>
