<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Covoiturons !</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy|Roboto" rel="stylesheet">

</head>
<body>
@if (Auth::user() && Auth::user()->rank === 1)
    <a id="welcomeLink" href="{{ url('/welcome') }}">Welcome</a>
@endif
<div id="app">
    <nav class="red darken-3 navbar z-depth-5 navbar-expand-md navbar-light navbar-laravel bouge">
        <div class="container">
            <img class="logo"
                 src="{{url('media/logzut.png')}}"
                 alt="computer on desktop" />
            <a class="navbar-brand" href="{{ url('/') }}">
                Pourquoi Covoiturer ?
            </a>
            <a class="navbar-brand" href="{{ url('/') }}">
                Charte

            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Authentication Links -->
                @guest
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Se Connecter') }}</a>
                    @if (Auth::user() && Auth::user()->rank === 1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __("S'inscrire") }}</a>
                        </li>
                    @endif
                @else



                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a href="{{url('/profile')}}">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</a>
                        @if (Auth::user() && Auth::user()->rank === 1)

                        @endif
                    </div>


                    <div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                @endguest

            </div>
        </div>
    </nav>

    <main class="py-4 contenu">
        @yield('content')
    </main>
</div>
</body>
</html>
