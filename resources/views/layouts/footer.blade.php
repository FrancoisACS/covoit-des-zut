<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Covoiturons !</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href=" {{ asset('/css/app.css')}}">
    </head>
    <body>
        <footer class="page-footer" style="background-color:#027A70;">
            <div class="row" style="margin: 0;">
                <div class="col s1">
                    <a href="#" class="black-text text_footer" id="link"><span class="red-text darken-3 text_footer">Con</span>tact</a>
                </div>
                <div class="separator left">
                   
                </div>
                <div class="col s1">  
                    <a href="#"class="black-text text_footer" id="link"><span class="red-text darken-3 text_footer">Sou</span>tien</a>
                </div>
                <div class="separator left">
                   
                </div>
                <div class="col s1">
                    <a href="#"class="black-text text_footer" id="link" ><span class="red-text darken-3 text_footer">Par</span>tenaires</a>
                </div>
                <div>
                    
                </div>
                <div class=" row col s8">
                    <a href="#" class="right black-text text_footer">Projet réalisé par Adrien.<span class="red-text darken-3 text_footer">A</span> , Francois.<span class="red-text darken-3 text_footer">C</span> , Anais.<span class="red-text darken-3 text_footer">D</span> et Julien.<span class="red-text darken-3 text_footer">O</span></a>
                </div>
            </div>
            <main class="py-4">
            @yield('footer')
            </main>
        </footer>

    </body>
</html>


