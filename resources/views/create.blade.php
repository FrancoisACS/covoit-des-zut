@extends('layouts.footer')

@extends('layouts.app')

@section('content')

    <!-- HAUT DE PAGE -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems, options);
        });
    </script>
    <div class="purple input-field col s6">
        <form action="{{ url('createTravel') }}" method="POST">
            <select name="ville1" class="blue lighten-4">
                <option value="" disabled selected>Ville départ</option>
                
               @foreach ($station as $id)
               <option value="{{$id->id_station}}">{{$id->station_name}} - {{$id->city}}</option>
               @endforeach
               
            </select>

            <select name="ville2" class="blue lighten-4">
                <option value="" disabled selected>Ville arrivé</option>
                @foreach ($station as $id)
                <option value="{{$id->id_station}}">{{$id->station_name}} - {{$id->city}}</option>
               @endforeach
            </select>

            <input type="time" id="time" name="time" placeholder="heure de départ">

            <input type="number" min="0" max="6" name="places" placeholder="nombre de places" />
            <input type="date" name="date" placeholder="date" />
            <button class="btn waves-effect waves-light" type="submit" name="action">validation
                <input type="hidden" name="_token" value="iIjW9PMNsV6VKT2sIc16ShoTf6SdYVZolVUGsxDI">
                <i class="material-icons right">send</i>
            </button>
            {{ csrf_field() }}
        </form>
    </div>

    @yield('footer')

@endsection
