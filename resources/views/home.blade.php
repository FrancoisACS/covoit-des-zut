@extends('layouts.footer')

@extends('layouts.app')

@section('content')


    <!-- HAUT DE PAGE -->
    <div style="background-image:url('../../../media/soleil.jpg'); background-color:black; background-size:100% 100%; background-repeat:no-repeat; width:100%; height:300px;">
        <img src="{{url('media/zut.png')}}" class="image_top left" alt="logo">
        <h4 class="center_align white-text round_font slogan">Plateforme de Covoiturage Gratuit des Amognes</h4>
        <p class="right white-text description">Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v.</p>
    </div>
    <div class="blue lighten-4 row menu">
        <div class="col s3 center-align card-panel hoverable choice_button" style="background-color: #027A70;">
            <a style="color:black;" href="{{ url('/createTravel') }}">
                <div style="height: 100%;">
                    <i class="large material-icons icon-button">directions_car</i>
                    <h3 class="menu_tiles round_font">Proposer un Trajet</h3>
                </div>
            </a>
        </div>
        <div class="col s3 center-align card-panel hoverable choice_button" style="background-color: #027A70;">
            <a style="color:black;" href="{{ url('/reserve') }}">
                <div style="height: 100%;">
                    <i class="large material-icons icon-button">search</i>
                    <h3 class="menu_tiles round_font">Rechercher un Trajet</h3>
                </div>
            </a>
        </div>
        <div class="col s3 center-align card-panel hoverable choice_button" style="background-color: #027A70;">
            <a style="color:black;" href="{{ url('/maps') }}">
                <div style="height: 100%;">
                    <i class="large material-icons icon-button">remove_red_eye</i>
                    <h3 class="menu_tiles round_font">Voir un Trajet</h3>
                </div>
            </a>
        </div>
    </div>

    <!-- BAS DE PAGE -->
    <div>
        <div style="background-image:url('../../../media/car.jpg'); background-color:black; background-size:100% 100%; background-repeat:no-repeat; width:100%; height:300px;">
            <h3 class="center-align white-text round_font date_display">Nous sommes le <?php
                setlocale (LC_TIME, 'fr_FR.utf8','fra');
                echo (strftime("%A %e %B %Y")); ?></h3>
        </div>
    </div>

    <div>
        <div class="indigo center-align darken-4 menu_jour">
            <h4 class="round_font">Derniers Trajets</h4>
            <div class="col s3 blue lighten-4 card-panel hoverable choice_travel">
                <h4 class="round_font">Ville 1 --> Ville 2</h4>
                <i class="small material-icons">question_answer</i>
            </div>
            <div class="col s3 blue lighten-4 card-panel hoverable choice_travel">
                <h4 class="round_font">Ville 1 --> Ville 2</h4>
                <i class="small material-icons">question_answer</i>
            </div>
            <div class="col s3 blue lighten-4 card-panel hoverable choice_travel">
                <h4 class="round_font">Ville 1 --> Ville 2</h4>
                <i class="small material-icons">question_answer</i>
            </div>
        </div>
    </div>
    
    @yield('footer')

@endsection
