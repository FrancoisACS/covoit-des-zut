<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/maps', function () {
    return view('maps');
});

Route::get('/profil', function () {
    return view('profil');
});

Route::get('/create', function () {
    return view('create');
});

Route::get('/reserve', function () {
    return view('reserve');
});

Route::get('/welcome', "Controller@index");

Auth::routes();

Route::get('createTravel', 'CreateController@create');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('reserve', 'ReserveController@store');

Route::post('createTravel', 'CreateController@store');
