<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth; 

class Controller extends BaseController
{
    public function index(){
        if (Auth::user() && Auth::user()->rank === 1){
            return view ('welcome'); 
    }
    abort(403,"vous n'êtes pas connectés");
}
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
