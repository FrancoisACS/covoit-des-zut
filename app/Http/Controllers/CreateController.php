<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class CreateController extends Controller
{
    public function create()
    {
            
            
        $station = DB::table('station')
            ->join('city', 'station.id_city', '=', 'city.id_city')
            ->select('id_station', 'station_name', 'city')
            ->get();

        return view('create', ['station' => $station]);
    }

    public function store(Request $request)
    {
        if (Auth::user())
        {
            
            $date = date_create($request->input('date').$request->input('time'));
            
            
            $travel = DB::table('travel')->insertGetId(
            ['start_time' => $date, 'place_number' => $_POST['places'], 'id_users' =>Auth::user()->id]
            );
            
            $nbtravel = DB::table('travel')->max('id_travel');
            
             $passed = DB::table('passed')->insert(
            ['id_station' =>$_POST['ville1'], 'id_travel' =>$nbtravel]
            );
            
             $passed = DB::table('passed')->insert(
            ['id_station' =>$_POST['ville2'], 'id_travel' =>$nbtravel]
            );
            
            return 'ok';
            
            
//            return 'Vous réservez ' . $request->input('ville1') . ' places '. $request->input('ville2') . ' places '.$request->input('time') . ' places '.$request->input('umessage') . ' places '.$request->input('places') . ' places '.$request->input('date') . ' places ' .Auth::user()->id;
        }
        else{
            return 'erreur';
        }
    }
}
